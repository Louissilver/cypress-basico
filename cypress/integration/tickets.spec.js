describe("Tickets", () => {
  beforeEach(() => cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html"));

  it("Preenche todos os campos de input de texto", () => {
    const firstName = "Luís"
    const lastName = "Silveira"

    cy.get("#first-name").type(firstName);
    cy.get("#last-name").type(lastName);
    cy.get("#email").type("lsilveira@example.com");
    cy.get("#requests").type("Vegetariano");
    cy.get("#signature").type(`${firstName} ${lastName}`);
  });

  it("Selecionar ticket '2'", () => {
    cy.get("#ticket-quantity").select("2");
  });

  it("Selecionar radio button 'VIP'", () => {
    cy.get("#vip").check();
  });

  it("Selecionar check de 'Social Media'", () => {
    cy.get("#social-media").check();
  });

  it("Selecionar 'Friend' e 'Publication', então desmarcar 'Friend'", () => {
    cy.get("#friend").check();
    cy.get("#publication").check();
    cy.get("#friend").uncheck();
  });

  it("Possui o título 'TICKETBOX' no cabeçalho da página", () => {
    cy.get("header h1").should("contain", "TICKETBOX");
  });

  it("Alerta de e-mail inválido", () => {
    cy.get("#email")
      .as("email")
      .type("lsilveira.com");

    cy.get("#email.invalid")
      .should("exist");

    cy.get("@email")
      .clear()
      .type("lsilveira@example.com");

    cy.get("#email.invalid")
      .should("not.exist");
  });

  it("Preencher todos os campos e depois resetar o formulário", () => {
    const firstName = "Luís";
    const lastName = "Silveira";
    const fullName = `${firstName} ${lastName}`;

    cy.get("#first-name").type(firstName);
    cy.get("#last-name").type(lastName);
    cy.get("#email").type("lsilveira@example.com");
    cy.get("#ticket-quantity").select("2");
    cy.get("#vip").check();

    cy.get(".agreement p").should(
      "contain",
      `I, ${fullName}, wish to buy 2 VIP tickets. I understand that all ticket sales are final.`
    );

    cy.get("#social-media").check();
    cy.get("#friend").check();
    cy.get("#publication").check();
    cy.get("#requests").type("Vegetariano");
    cy.get('#agree').check();
    cy.get("#signature").type(fullName);

    cy.get("button[type='submit']")
      .as("submitButton")
      .should("not.be.disabled");

    cy.get("button.reset").click();

    cy.get("@submitButton")
      .should("be.disabled");
  });

  it("Preencher os campos obrigatórios utilizando comandos customizados", () => {
    const customer = {
      firstName: "Luís",
      lastName: "Silveira",
      email: "lsilveira@example.com"
    };

    cy.preencherCamposObrigatorios(customer);

    cy.get("button[type='submit']")
      .as("submitButton")
      .should("not.be.disabled");

    cy.get("#agree").uncheck();

    cy.get("@submitButton")
      .should("be.disabled");
  });
});

